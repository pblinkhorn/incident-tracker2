import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// load in the project's components
import { IncidentsComponent } from './components/incidents/incidents.component';
import { ProfilesComponent } from './components/profiles/profiles.component';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import { Profiles } from './profile';
import { AppComponent } from './app.component';

const routes: Routes = [
  { path: 'incidents', component: IncidentsComponent },
  { path: 'profiles', component: ProfilesComponent },
  { path: 'user-settings', component: UserSettingsComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
