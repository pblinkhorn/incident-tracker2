import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const incidents = [
      { id: 11, type: 'traffic', description: 'Traffic stop, illegal left turn', date: '05/23/2018', address: '21000 Atlantic Blvd' },
      { id: 12, type: 'domestic', description: 'Violation of paroll. Crossing state lines.', date: '04/01/2018', address: '123 New Street' },
      { id: 13, type: 'domestic', description: 'Arrest, armed robbery.', date: '02/19/2018', address: '878 Fake Rd' },
      { id: 14, type: 'domestic', description: 'Domestic disturbance at residence.', date: '06/20/2018', address: '449 Awesome Rd' },
      { id: 15, type: 'domestic', description: 'Traffic stop, failure to follow stop sign.', date: '09/09/2018', address: '777 Forever St' }
    ];
    return {incidents};
  }
}
