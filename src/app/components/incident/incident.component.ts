
//// MODULES AND DEPENDENCIES ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

import { Component } from '@angular/core';



//// COMPONENT METADATA /////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

@Component({
  selector: 'app-incident',
  templateUrl: './incident.component.html',
  styleUrls: ['./incident.component.css']
})



//// COMPONENT //////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

export class IncidentComponent {

  //// declare properties with types ////////////////////////////////////
  id: number;
  description: string;
  type: string;
  date: string;
  address: string;

  constructor() { }

}




//// END ////////////////////////////////////////////////////////////////
