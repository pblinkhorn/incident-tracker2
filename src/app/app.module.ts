import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { IncidentsComponent } from './components/incidents/incidents.component';
import { IncidentComponent } from './components/incident/incident.component';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

// this is just temporary to simluate a server
import { InMemoryDataService }  from './in-memory-data.service';


import { NavComponent } from './components/nav/nav.component';

import { ProfilesComponent } from './components/profiles/profiles.component';
import { AppRoutingModule } from './/app-routing.module';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import { DummyComponent } from './dummy/dummy.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ProfilesComponent,
    IncidentsComponent,
    IncidentComponent,
    UserSettingsComponent,
    DummyComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot( InMemoryDataService, {
      dataEncapsulation: false
    }),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})


export class AppModule { }
